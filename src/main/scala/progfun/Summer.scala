package progfun

import progfun.ChecksumAccumulator.calculate

object Summer {
  def main(args: Array[String]) {
    for (a <- args)
      println(a + " = " + calculate(a))
  }
}