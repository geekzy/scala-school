class Rational(n: Int, d: Int) {
  require(d != 0)
  private val g = gcd(n.abs, d.abs)
  val numer: Int = n / g
  val denom: Int = d / g

  def this(n: Int) = this(n, 1)

  def this() = this(1)

  def gcd(x: Int, y: Int): Int =
    if (y == 0) x else gcd(y, x % y)

  override def toString: String = numer + "/" + denom

  def +(that: Rational): Rational =
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom
    )

  def +(i: Int): Rational =
    new Rational(numer + i * denom, denom)

  def -(that: Rational): Rational =
    new Rational(
      numer * that.denom - that.numer * denom,
      denom * that.denom
    )

  def -(i: Int): Rational =
    new Rational(numer - i * denom, denom)

  def *(that: Rational): Rational =
    new Rational(numer * that.numer, denom * that.denom)

  def *(i: Int): Rational =
    new Rational(numer * i, denom)

  def /(that: Rational): Rational =
    new Rational(numer * that.denom, denom * that.numer)

  def /(i: Int): Rational =
    new Rational(numer, denom * i)

  def lessThan(that: Rational): Boolean =
    numer * that.denom < that.numer * denom

  def max(that: Rational): Rational =
    if (lessThan(that)) that else this
}

val a = new Rational(1, 2)
a.numer
val b = new Rational(2, 3)
b.denom
a + b
a lessThan b
a max b
val c = new Rational(5)      // implicitly new Rational(5, 1)
val d = new Rational         // implicitly new Rational(1, 1)
val e = new Rational(66, 42)
a * b
a + a * b
(a + a) * b
a + (a * b)
c * 2 / 2 + 1 - a
d lessThan c
c lessThan e

implicit def intToRational(x: Int): Rational = new Rational(x)

2 * a
