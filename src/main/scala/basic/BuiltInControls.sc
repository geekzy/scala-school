import java.io.File

import scala.io.Source

val base = "/Users/imam/dev/repos/scala/scala-school/script"

val filesHere = new File(base).listFiles()

for (f <- filesHere if f.getName.endsWith(".scala"))
  println(f.getName)

for (
  f <- filesHere
  if f.isFile
  if f.getName.endsWith(".sc")
) println(f.getName)

def fileLines(file: File) =
  Source.fromFile(file).getLines().toList

def grep(pattern: String) =
  for {
    f <- filesHere
    if f.getName.endsWith(".scala")
    line <- fileLines(f)
    trimmed = line.trim
    if trimmed.matches(pattern)
  } yield (f, trimmed)

grep(".*while.*").foreach(
  r => println(r._1.getName + ": " + r._2)
)

def half(n: Int) =
  if (n % 2 == 0) n / 2
  else throw new RuntimeException("n must be even")

half(4)
half(6)
try {
  half(9)

} catch {
  case ex: RuntimeException =>
    println(ex.getMessage)
}

val args: Array[String] = Array("salt", "chips", "eggs")
val firstArgs: String = if (!args.isEmpty) args(0) else ""

val friend = firstArgs match {
  case "salt" => "peper"
  case "chips" => "salsa"
  case "eggs" => "bacon"
  case _ => "huh?"
}
println(friend)

val a = 1; {
  val a = 2
  println("a is " + a)
}
println("a is " + a)

def printMultitable(): Unit = {
  var i = 1
  while (i < 10) {
    var j = 1
    while (j < 10) {
      val prod = (i * j).toString
      var k = prod.length
      while (k < 4) {
        print(" ")
        k += 1
      }
      print(prod)
      j += 1
    }
    println()
    i += 1
  }
}

printMultitable()


def multiTable() = {

  def makeRowSeq(row: Int) =
    for (col <- 1 to 10) yield {
      val prod = (row * col).toString
      val pad = " " * (4 - prod.length)
      pad + prod
    }

  def makeRow(row: Int) = makeRowSeq(row).mkString

  val tableSeq = for (row <- 1 to 10) yield makeRow(row)
  tableSeq.mkString("\n")
}

println(multiTable())