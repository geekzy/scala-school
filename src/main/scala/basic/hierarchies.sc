println("Hello I'm with Scala")

abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean

  def union(other: IntSet): IntSet
}

object Empty extends IntSet {
  def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty)

  def contains(x: Int): Boolean = false

  def union(other: IntSet): IntSet = other

  override def toString: String = "."
}

class NonEmpty(elm: Int, left: IntSet, right: IntSet) extends IntSet {
  def incl(x: Int): IntSet =
    if (x < elm) new NonEmpty(elm, left incl x, right)
    else if (x > elm) new NonEmpty(elm, left, right incl x)
    else this

  def contains(x: Int): Boolean =
    if (x < elm) left contains x
    else if (x > elm) right contains x
    else true

  def union(other: IntSet): IntSet = ((left union right) union other) incl elm

  override def toString = "{" + left + elm + right + "}"
}

val t1 = new NonEmpty(3, Empty, Empty)
val t2 = t1 incl 4

val o1 = new NonEmpty(5, Empty, Empty)
val o2 = o1 incl 2

val big = (((t2 union o2) incl 1) incl 10) incl 9
big contains 10
big contains 6