


// a value

val x = 1

// a variable

var y = "Me"

y = "You"

// a function

def three() = 1 + 2

// invoke a function

three()

// anonymous function

(x: Int) => x + 2

// immediate invoke of anonymous function

((x: Int) => x + 2)(3)

// assign anonymous function to a value

val addOne = (x: Int) => x + 1

// invoke addOne

addOne(1)

// with multiline expression (block)

def timesTwo(i: Int) = {
  println(i + " times Two")
  i * 2
}

println("is " + timesTwo(4))


// partial application

def adder(x: Int, y: Int) = x + y

val addThree = adder(3, _:Int)

// invoke the partial

addThree(5)

// curried function

def multiply(m: Int)(n: Int): Int = m * n

// invoke the curried function

multiply(5)(8)

// partially apply second param

val times5 = multiply(5)_

times5(6)

val curriedAdd = (adder _).curried

val addTwo = curriedAdd(2)

addTwo(5)

def capitalizeAll(args: String*) = {
  args.map(arg => arg.capitalize)
}

capitalizeAll("rarity", "applyjack")

abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
}

class NoEmpty(elm: Int, left: IntSet, right: IntSet) extends IntSet {
  override def incl(x: Int): IntSet = {
    if (x < elm) new NoEmpty(elm, left incl x, right)
    else if (x > elm) new NoEmpty(elm, left, right incl x)
    else this
  }

  override def contains(x: Int): Boolean = {
    if (x < elm) left contains x
    else if (x > elm) right contains x
    else true
  }
}
