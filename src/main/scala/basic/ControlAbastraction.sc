println("Hello World!")

object FileMatcher {
  private def filesHere = new java.io.File(".").listFiles

  def filesEnding(query: String) = {
    for (file <- filesHere; if file.getName.endsWith(query))
      yield file
  }

  def filesContain(query: String) = {
    for (file <- filesHere; if file.getName.contains(query))
      yield file
  }

  def filesRegex(regex: String) = {
    for (file <- filesHere; if file.getName.matches(regex))
      yield file
  }
}

FileMatcher.filesEnding("scala")

FileMatcher.filesContain("B")

def curriedSum(x: Int)(y: Int) = x + y

def theSum(x: Int) = (y: Int) => x + y

curriedSum(2)(3)

val twoFn = curriedSum(2) _

twoFn(8)

val fiveFn = theSum(5)

fiveFn {
  10
}

def twice(op: Double => Double, x: Double) = op(op(x))

twice(_ + 1, 5)

def tw(d: Double) = d + 1

twice(tw, 5)


