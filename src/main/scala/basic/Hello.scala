package basic

/**
  * @author Imam Kurniawan (geekzy@gmail.com)
  */
object Hello {
  def main(args: Array[String]): Unit = println("Hello World!")
}
