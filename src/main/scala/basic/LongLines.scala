package basic

import scala.io.Source

/**
  * Fine long lines of a predefined line length and filename
  * $ scala FindLongLines 45 src/main/scala/basic/LongLines.scala
  *
  * @author Imam Kurniawan (geekzy@gmail.com)
  */
object LongLines {
  def processFile(filename: String, length: Int): Unit = {

    def processLine(line: String) =
      if (line.length > length && !line.trim.startsWith("*"))
        println(filename + ": " + line.trim)

    val source = Source.fromFile(filename)
    for (line <- source.getLines())
      processLine(line)
  }
}

object FindLongLines {
  def main(args: Array[String]): Unit = {
    val width = args(0).toInt
    for (arg <- args.drop(1))
      LongLines.processFile(arg, width)
  }
}


object PrintLines {
  def main(args: Array[String]): Unit = {

    def widthOfLenght(s: String) = s.length.toString.length

    if (args.length > 0) {
      val lines = Source.fromFile(args(0)).getLines().toList

      val longestLine = lines.reduceLeft(
        (a, b) => if (a.length > b.length) a else b
      )

      val maxWidth = widthOfLenght(longestLine)

      for ((line, i) <- lines.view.zipWithIndex) {

        val numSpace = maxWidth - widthOfLenght(line)

        val lineSpace = lines.length.toString.length - (i + 1).toString.length

        val padding = " " * numSpace

        val linePad = " " * lineSpace

        println(linePad + (i + 1) + "| " + padding + line.length + "] " + line)
      }
      /*var i = 0
      lines.foreach(
        line => {
          i = i + 1

          val numSpace = maxWidth - widthOfLenght(line)

          val lineSpace = lines.length.toString.length - i.toString.length

          val padding = " " * numSpace

          val linePad = " " * lineSpace

          println(linePad + i + "| " + padding + line.length + "] " + line)
        }
      )*/
    }
    else Console.err.println("Please enter filename")
  }

}