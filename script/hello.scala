// println("Hello "+ args(0) +"! from a script")
var i = 0
while (i < args.length) {
  println(args(i))
  i += 1
}

val greetString = new Array[String](3)
println

greetString(0) = "Hello"
greetString(1) = ", "
greetString(2) = "World!"

for (i <- 0 to 2) print(greetString(i))
println