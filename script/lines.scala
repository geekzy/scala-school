import scala.io.Source

def valueLength(n: Int) = n.toString.length

if (args.length > 0) {
  val lines = Source.fromFile(args(0)).getLines.toList
  //val linesIndexed = lines.view.zipWithIndex
  val maxLine = lines.reduceLeft((a, b) => if (a.length > b.length) a else b)

  lines.foreach((line) => {
    val width = valueLength(maxLine.length) - valueLength(line.length)
    val pad = " " * width
    println(pad + line.length + " | " + line)
    })
  /**
  linesIndexed.foreach(v => {
    val line = v._1
    val i = v._2 + 1
    val spaces = valueLength(lines.size) - valueLength(i)
    val pad = " " * spaces
    println(pad + i + " | " + line)
    })*/
}
else Console.err.println("Please enter filename")